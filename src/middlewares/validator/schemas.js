import Joi from 'joi';
import joiObjectId from 'joi-objectid';

Joi.objectId = joiObjectId(Joi);

export const idValidationSchema = Joi.object({
  id: Joi.objectId().required(),
});

export const userValidationSchema = Joi.object()
    .keys({
      email: Joi.string()
          .email()
          .required(),
      password: Joi.string(),
      role: Joi.string()
          .required(),
    });

export const truckTypeValidationSchema = Joi.object({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
});

export const loadCreateValidationSchema = Joi.object()
    .keys({
      'name': Joi
          .string()
          .required(),
      'payload': Joi
          .number()
          .min(0)
          .required(),
      'pickup_address': Joi
          .string()
          .required(),
      'delivery_address': Joi
          .string()
          .required(),
      'dimensions': {
        'width': Joi
            .number()
            .min(0)
            .required(),
        'length': Joi
            .number()
            .min(0)
            .required(),
        'height': Joi
            .number()
            .min(0)
            .required(),
      },
    });

export const loadsGetValidationSchema = Joi.object().keys({
  status: Joi
      .string()
      .optional()
      .valid(
          'NEW',
          'POSTED',
          'ASSIGNED',
          'SHIPPED',
      ),
  limit: Joi
      .number()
      .optional()
      .min(0)
      .max(50),
  offset: Joi
      .number()
      .optional()
      .min(0),
});

export const loadsGetDriverValidationSchema = Joi.object().keys({
  status: Joi
      .string()
      .optional()
      .valid(
          'ASSIGNED',
          'SHIPPED',
      ),
  limit: Joi
      .number()
      .optional()
      .min(0)
      .max(50),
  offset: Joi
      .number()
      .optional()
      .min(0),
});

export const loadUpdateValidationSchema = Joi.object()
    .keys({
      'name': Joi
          .string()
          .optional(),
      'payload': Joi
          .number()
          .min(0)
          .optional(),
      'pickup_address': Joi
          .string()
          .optional(),
      'delivery_address': Joi
          .string()
          .optional(),
      'dimensions': {
        'width': Joi
            .number()
            .min(0)
            .optional(),
        'length': Joi
            .number()
            .min(0)
            .optional(),
        'height': Joi
            .number()
            .min(0)
            .optional(),
      },
    });
