import {BadRequestError} from '../../errors/errors';

const validator = (schema, property) => {
  return (req, res, next) => {
    const {error} = schema.validate(req[property]);

    if (error) {
      throw new BadRequestError(error.message);
    } else {
      return next();
    }
  };
};

export default validator;
