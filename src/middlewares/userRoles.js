import {BadRequestError} from '../errors/errors';

export const allowRoles = (roles) => {
  return (req, res, next) => {
    const {role} = res.locals.auth;

    if (!role || !(roles.includes(role))) {
      throw new BadRequestError(`Available only for ${roles} user role(s)`);
    }

    return next();
  };
};
