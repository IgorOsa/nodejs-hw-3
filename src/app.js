import express from 'express';
import swaggerUI from 'swagger-ui-express';
import authChecker from './auth/authChecker';
import {swaggerDocument} from './common/config';
import {errorHandler} from './errors/errorHandler';
import logger from './common/logger';
import authRouter from './resources/auth/auth.router';
import usersRouter from './resources/users/users.router';
import trucksRouter from './resources/trucks/trucks.router';
import loadsRouter from './resources/loads/loads.router';
import {allowRoles} from './middlewares/userRoles';

const app = express();

app.use(express.json());

app.use('/doc', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

const greeting = {message: 'Welcome to Uber Track API server!'};

app.use(logger.logToConsole);

app.get('/api', (req, res, next) => {
  if (req.originalUrl === '/api') {
    res.send(greeting);
    return;
  }
  next();
});

app.use('/api/auth', authRouter);
app.use('/api/users', authChecker, usersRouter);
app.use('/api/trucks', authChecker, allowRoles(['DRIVER']), trucksRouter);
app.use('/api/loads', authChecker, loadsRouter);

app.use(errorHandler);

export default app;
