/* eslint-disable camelcase */
import {BadRequestError} from '../../errors/errors';
import Truck from './trucks.model';

const create = async (truck) => {
  await Truck.create(truck);
};

const getAll = async () => {
  return await Truck.find({});
};

const get = async ({id}) => {
  try {
    return await Truck.findOne({_id: id});
  } catch (err) {
    throw new BadRequestError('Wrong truck id');
  }
};

const getByFilter = async (filter) => {
  try {
    return await Truck.find(filter);
  } catch (err) {
    throw new BadRequestError('No trucks found');
  }
};

const update = async (truck, updates) => {
  try {
    const {id} = truck;
    await Truck.findOneAndUpdate({_id: id}, {...updates});
  } catch (error) {
    throw new BadRequestError();
  }
};

const remove = async (truck) => {
  try {
    const {id} = truck;
    const deleted = await Truck.deleteOne({_id: id});
    if (deleted.deletedCount !== 1) {
      throw new BadRequestError('No truck deleted');
    }
  } catch (err) {
    throw new BadRequestError('Error deleting truck');
  }
};

export default {
  create,
  getAll,
  get,
  getByFilter,
  update,
  remove,
};
