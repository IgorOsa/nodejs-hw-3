import mongoose from 'mongoose';

const truckSchema = new mongoose.Schema(
    {
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
        default: '',
      },
      type: {
        type: String,
        enum: [
          'SPRINTER',
          'SMALL STRAIGHT',
          'LARGE STRAIGHT',
        ],
      },
      status: {
        type: String,
        enum: ['OL', 'IS'],
        default: 'IS',
      },
      created_date: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: 'trucks',
      versionKey: false,
    },
);

const Truck = mongoose.model('Truck', truckSchema);

export default Truck;
