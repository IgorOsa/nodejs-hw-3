/* eslint-disable camelcase */
import {BadRequestError} from '../../errors/errors';
import trucksDbRepo from './trucks.db.repository';

const create = async (truck) => trucksDbRepo.create(truck);

const getAll = async () => trucksDbRepo.getAll();

const get = async (truck) => trucksDbRepo.get(truck);

const getByFilter = async (filter) => trucksDbRepo.getByFilter(filter);

const update = async (truck, updates) => trucksDbRepo.update(truck, updates);

const assignDriver = async (id, userId) => {
  const assigned = await trucksDbRepo.getByFilter({assigned_to: userId});

  if (assigned.length) {
    throw new BadRequestError('Driver can assign only one truck to himself');
  }
  return await trucksDbRepo.update({id}, {assigned_to: userId});
};

const updateNotAssigned = async ({id, userId}, updates) => {
  const truckData = await get({id});

  if (!truckData) {
    throw new BadRequestError(`Wrong truck id`);
  }

  const {assigned_to} = truckData;

  if (assigned_to === userId) {
    throw new BadRequestError(`Can't update assigned to you truck`);
  }

  await trucksDbRepo.update({id}, updates);
};

const removeNotAssigned = async ({id, userId}, updates) => {
  const truckData = await get({id});

  if (!truckData) {
    throw new BadRequestError(`Wrong truck id or truck already deleted`);
  }

  const {assigned_to} = truckData;

  if (assigned_to === userId) {
    throw new BadRequestError(`Can't delete assigned to you truck`);
  }

  await trucksDbRepo.remove({id});
};

const getISTrucksWithDrivers = async ({payload, dimensions}) => {
  let {width, length, height} = dimensions;

  const trucksVolumes = [
    {
      name: 'SPRINTER',
      width: 300,
      length: 250,
      height: 170,
      payload: 1700,
    },
    {
      name: 'SMALL STRAIGHT',
      width: 500,
      length: 250,
      height: 170,
      payload: 2500,
    },
    {
      name: 'LARGE STRAIGHT',
      width: 700,
      length: 350,
      height: 200,
      payload: 4000,
    },
  ];

  const volume = [width, length, height].sort((a, b) => b - a);

  [width, length, height] = volume;

  const filteredTrucks = trucksVolumes.filter((el) => {
    return el.width > width && el.height > height && el.length > length;
  });

  const regexString = new RegExp(filteredTrucks.map((el) => el.name).join('|'));

  const filter = {
    status: 'IS',
    type: regexString,
    assigned_to: {$ne: ''},
  };
  const trucks = await trucksDbRepo.getByFilter(filter);

  return trucks;
};

export default {
  assignDriver,
  create,
  getAll,
  get,
  getByFilter,
  update,
  updateNotAssigned,
  removeNotAssigned,
  getISTrucksWithDrivers,
};
