import express from 'express';
import asyncWrapper from '../../common/asyncWrapper';
import trucksService from './trucks.service';
import validator from './../../middlewares/validator/validator';
import {
  idValidationSchema,
  truckTypeValidationSchema,
} from './../../middlewares/validator/schemas';

const router = new express.Router();

router.route('/')
    .get(
        asyncWrapper(async (req, res) => {
          const trucks = await trucksService.getAll();
          res.json({trucks});
        }),
    )
    .post(
        validator(truckTypeValidationSchema, 'body'),
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const {type} = req.body;
          await trucksService.create({created_by: userId, type});
          res.json({message: 'Truck created successfully'});
        }),
    );

router.route('/:id')
    .get(
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          const truck = await trucksService.get({id});
          res.json({truck});
        }),
    )
    .put(
        validator(idValidationSchema, 'params'),
        validator(truckTypeValidationSchema, 'body'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          const {userId} = res.locals.auth;
          const {type} = req.body;
          await trucksService.updateNotAssigned({id, userId}, {type});
          res.json({message: 'Truck details changed successfully'});
        }),
    )
    .delete(
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          const {userId} = res.locals.auth;
          await trucksService.removeNotAssigned({id, userId});
          res.json({message: 'Truck deleted successfully'});
        }),
    );

router.route('/:id/assign')
    .post(
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          const {userId} = res.locals.auth;
          await trucksService.assignDriver(id, userId);
          res.json({message: 'Truck assigned successfully'});
        }),
    );


export default router;
