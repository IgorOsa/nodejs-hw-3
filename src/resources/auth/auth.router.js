import express from 'express';
import asyncWrapper from '../../common/asyncWrapper';
import authService from './auth.service';

const router = new express.Router();

router.route('/register').post(
    asyncWrapper(async (req, res) => {
      await authService.create(req.body);
      res.json({message: 'Profile created successfully'});
    }),
);

router.route('/login').post(
    asyncWrapper(async (req, res) => {
      const token = await authService.login(req.body);
      res.json({jwt_token: token});
    }),
);

export default router;
