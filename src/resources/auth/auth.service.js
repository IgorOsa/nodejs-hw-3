import jwt from 'jsonwebtoken';
import Credentials from './../auth/credentials.model';
import {BadRequestError} from '../../errors/errors';
import {checkHashedPassword} from '../../auth/hashHelpers';
import {JWT_SECRET_KEY, JWT_EXPIRE_TIME} from '../../common/config';
import usersService from '../users/users.service';
import usersDbRepository from '../users/users.db.repository';

const create = (user) => {
  const {email, password} = user;
  if (!email || !password) {
    throw new BadRequestError();
  }
  return usersService.create(user);
};

const login = async (user) => {
  try {
    const {email, password} = user;
    const userEntity = await Credentials.findOne({email}).exec();

    if (!userEntity) {
      throw new BadRequestError();
    }

    const comparisonRes = await checkHashedPassword(
        password,
        userEntity.password,
    );

    if (comparisonRes) {
      const {_id} = userEntity;
      const {role} = await usersDbRepository.get({email});

      const token = jwt.sign({userId: _id, email, role}, JWT_SECRET_KEY, {
        expiresIn: JWT_EXPIRE_TIME,
      });
      return token;
    }

    throw new BadRequestError();
  } catch (err) {
    throw new BadRequestError();
  }
};

export default {login, create};
