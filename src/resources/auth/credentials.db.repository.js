import {checkHashedPassword} from '../../auth/hashHelpers';
import {BadRequestError} from '../../errors/errors';
import Credentials from './credentials.model';

const create = async (user) => {
  try {
    return await Credentials.create(user);
  } catch (err) {
    throw new BadRequestError();
  }
};

const update = async (user) => {
  const {email, password, oldPassword} = user;

  const userEntity = await Credentials.findOne({email}).exec();

  if (!userEntity) {
    throw new BadRequestError();
  }

  const comparisonRes = await checkHashedPassword(
      oldPassword,
      userEntity.password,
  );

  if (comparisonRes) {
    const updatedUser = await Credentials.findOneAndUpdate(
        {email},
        {$set: {email, password}},
        {new: true},
    );

    if (!updatedUser) {
      throw new BadRequestError();
    }

    return updatedUser;
  }

  throw new BadRequestError();
};

const remove = async (user) => {
  const {email} = user;
  const deletedCredentials = await Credentials.deleteOne({email});

  if (deletedCredentials.deletedCount !== 1) {
    throw new BadRequestError();
  }

  return deletedCredentials;
};

export default {create, update, remove};
