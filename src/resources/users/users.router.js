import express from 'express';
import {BadRequestError} from '../../errors/errors';
import asyncWrapper from '../../common/asyncWrapper';
import usersService from './users.service';

const router = new express.Router();

router.route('/me')
    .get(
        asyncWrapper(async (req, res) => {
          const authData = res.locals.auth;
          const user = await usersService.get(authData);
          res.json({user});
        }),
    )
    .delete(
        asyncWrapper(async (req, res) => {
          const {email, userId} = res.locals.auth;
          await usersService.remove({email, userId});
          // also need to delete creted by user resources
          res.json({message: 'Success'});
        }),
    );

router.route('/me/password')
    .patch(
        asyncWrapper(async (req, res) => {
          const {email} = res.locals.auth;
          const {oldPassword, newPassword} = req.body;

          if (!email || !oldPassword || !newPassword) {
            throw new BadRequestError();
          }

          await usersService.update(
              {email, password: newPassword, oldPassword});
          res.json({message: 'Password changed successfully'});
        }),
    );

export default router;
