import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
    {
      role: {
        type: String,
        required: true,
        enum: ['DRIVER', 'SHIPPER'],
      },
      email: {
        type: String,
        unique: true,
        required: true,
      },
      created_date: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: 'users',
      versionKey: false,
    },
);

const User = mongoose.model('User', userSchema);

export default User;
