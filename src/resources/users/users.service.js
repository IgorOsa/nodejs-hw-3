import usersRepo from './users.db.repository';
import credentialsRepo from '../auth/credentials.db.repository';
import credentialsDbRepository from '../auth/credentials.db.repository';

const create = async (user) => {
  const userCreds = await credentialsRepo.create(user);
  const {_id} = userCreds;

  try {
    await usersRepo.create({...user, _id});
  } catch (err) {
    credentialsRepo.remove(userCreds);
    throw err;
  }
};

const get = async (user) => usersRepo.get(user);

const update = async (user) => credentialsDbRepository.update(user);

const remove = async (user) => usersRepo.remove(user);

export default {create, get, update, remove};
