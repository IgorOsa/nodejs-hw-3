import {BadRequestError} from '../../errors/errors';
import Credentials from '../auth/credentials.model';
import User from './users.model';

const create = async (user) => {
  try {
    return await User.create(user);
  } catch (err) {
    throw new BadRequestError();
  }
};

const get = async ({email}) => {
  try {
    const user = await User.findOne({email}).exec();

    if (!user || user === null) {
      throw new BadRequestError();
    }

    return user;
  } catch (err) {
    throw new BadRequestError();
  }
};

const remove = async ({email}) => {
  const deletedUser = await User.deleteOne({email});
  const deletedCredentials = await Credentials.deleteOne({email});
  if (deletedUser.deletedCount !== 1 || deletedCredentials.deletedCount !== 1) {
    throw new BadRequestError();
  }
  return deletedUser;
};

export default {create, get, remove};
