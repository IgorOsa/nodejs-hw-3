/* eslint-disable camelcase */
import express from 'express';
import asyncWrapper from '../../common/asyncWrapper';
import loadsService from './loads.service';
import {allowRoles} from '../../middlewares/userRoles';
import validator from '../../middlewares/validator/validator';
import {
  idValidationSchema,
  loadCreateValidationSchema,
  loadsGetDriverValidationSchema,
  loadsGetValidationSchema,
  loadUpdateValidationSchema,
} from '../../middlewares/validator/schemas';
import trucksService from '../trucks/trucks.service';

const router = new express.Router();

router.route('/')
    .get(
        (req, res, next) => {
          const {role} = res.locals.auth;
          if (role === 'DRIVER') {
            next('route');
          } else {
            next();
          }
        },
        allowRoles(['SHIPPER']),
        validator(loadsGetValidationSchema, 'query'),
        asyncWrapper(async (req, res) => {
          const status = req?.query?.status ||
            (new RegExp(/NEW|POSTED|ASSIGNED|SHIPPED/));
          const offset = req?.query?.offset || 0;
          const limit = req?.query?.limit || 10;
          const loads = await loadsService.getAll({status, offset, limit});
          res.json({loads});
        }),
    )
    .post(
        allowRoles(['SHIPPER']),
        validator(loadCreateValidationSchema, 'body'),
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          await loadsService.create({created_by: userId, ...req.body});
          res.json({message: 'Load created successfully'});
        }),
    );

router.route('/').get(
    allowRoles(['DRIVER']),
    validator(loadsGetDriverValidationSchema, 'query'),
    asyncWrapper(async (req, res) => {
      const status = req?.query?.status ||
            (new RegExp(/ASSIGNED|SHIPPED/));
      const offset = req?.query?.offset || 0;
      const limit = req?.query?.limit || 10;
      const loads = await loadsService.getAll({status, offset, limit});
      res.json({loads});
    }),
);

router.route('/active')
    .get(
        allowRoles(['DRIVER']),
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const [load] = await loadsService.getActiveLoad(userId);
          res.json({load});
        }),
    );

router.route('/active/state')
    .patch(
        allowRoles(['DRIVER']),
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const state = await loadsService.updateLoadActiveState(userId);
          res.json({message: `Load state changed to '${state}'`});
        }),
    );

router.route('/:id')
    .get(
        allowRoles(['SHIPPER']),
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          const load = await loadsService.get(id);
          res.json({load});
        }),
    )
    .put(
        allowRoles(['SHIPPER']),
        validator(idValidationSchema, 'params'),
        validator(loadUpdateValidationSchema, 'body'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          await loadsService.update({id}, {...req.body} );
          res.json({message: 'Load details changed successfully'});
        }),
    )
    .delete(
        allowRoles(['SHIPPER']),
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          await loadsService.remove({id});
          res.json({message: 'Load deleted successfully'});
        }),
    );

router.route('/:id/post')
    .post(
        allowRoles(['SHIPPER']),
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          await loadsService.postLoadById({id});
          res.json({message: 'Load posted successfully', driver_found: true});
        }),
    );

router.route('/:id/shipping_info')
    .get(
        allowRoles(['SHIPPER']),
        validator(idValidationSchema, 'params'),
        asyncWrapper(async (req, res) => {
          const {id} = req.params;
          const load = await loadsService.get(id);
          const {assigned_to} = load;
          const [truck] = await trucksService.getByFilter({assigned_to});
          res.json({load, truck});
        }),
    );

export default router;
