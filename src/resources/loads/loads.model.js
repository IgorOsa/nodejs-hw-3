import mongoose from 'mongoose';

export const LOAD_STATUSES = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const dimenshionSchema = new mongoose.Schema({
  width: Number,
  length: Number,
  height: Number,
},
{
  _id: false,
},
);

const logSchema = new mongoose.Schema({
  message: String,
  created_date: {
    type: Date,
    default: Date.now,
  },
},
{
  _id: false,
},
);

const loadSchema = new mongoose.Schema(
    {
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
        default: '',
      },
      status: {
        type: String,
        enum: [
          'NEW',
          'POSTED',
          'ASSIGNED',
          'SHIPPED'],
        default: 'NEW',
      },
      state: {
        type: String,
        enum: LOAD_STATUSES,
        default: 'En route to Pick Up',
      },
      name: {
        type: String,
      },
      payload: {
        type: Number,
      },
      pickup_address: {
        type: String,
      },
      delivery_address: {
        type: String,
      },
      dimensions: {
        type: dimenshionSchema,
      },
      logs: [logSchema],
      created_date: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: 'loads',
      versionKey: false,
    },
);

const Load = mongoose.model('Load', loadSchema);

export default Load;
