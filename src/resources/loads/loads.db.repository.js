/* eslint-disable camelcase */
import {BadRequestError} from '../../errors/errors';
import Load from './loads.model';

const create = async (load) => {
  await Load.create(load);
};

const getAll = async ({status, offset, limit}) => {
  return await Load.find({status}, null, {skip: +offset, limit: +limit}).exec();
};

const get = async (id) => {
  try {
    const load = await Load.findOne({_id: id});
    if (!load) {
      throw new BadRequestError(`Load with id ${id} not found`);
    }
    return load;
  } catch (err) {
    throw new BadRequestError(err.message);
  }
};

const getByFilter = async (filter) => {
  try {
    return await Load.find(filter);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
};

const update = async (load, updates) => {
  const {id} = load;
  try {
    const result = await Load.findOneAndUpdate(
        {_id: id},
        {...updates},
        {new: true},
    );

    if (!result) {
      throw new BadRequestError(`Can't update load id ${id} or wrong id`);
    }
    return result;
  } catch (err) {
    throw new BadRequestError(err.message);
  }
};

const remove = async (load) => {
  try {
    const {id} = load;
    const deleted = await Load.deleteOne({_id: id});
    if (deleted.deletedCount !== 1) {
      throw new BadRequestError(`Can't delete load id ${id} or wrong id`);
    }
  } catch (err) {
    throw new BadRequestError(err.message);
  }
};

export default {
  create,
  getAll,
  get,
  getByFilter,
  update,
  remove,
};
