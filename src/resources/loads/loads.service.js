/* eslint-disable camelcase */
import {BadRequestError} from '../../errors/errors';
import loadsDbRepo from './loads.db.repository';
import trucksService from '../trucks/trucks.service';
import {LOAD_STATUSES} from './loads.model';

const create = async (load) => loadsDbRepo.create(load);

const getAll = async (filter) => loadsDbRepo.getAll(filter);

const get = async (id) => loadsDbRepo.get(id);

const getActiveLoad = async (userId) => {
  const load = loadsDbRepo.getByFilter({
    assigned_to: userId,
    status: 'ASSIGNED',
  });

  if (!load) {
    throw new BadRequestError('No active load for you');
  }
  return load;
};

const update = async (load, updates) => {
  const {id} = load;
  const loadEntity = await get(id);

  if (loadEntity.status === 'NEW') {
    await loadsDbRepo.update(load, updates);
  } else {
    throw new BadRequestError(`You can update loads only with NEW status`);
  }
};

const updateLoadActiveState = async (userId) => {
  const [load] = await getActiveLoad(userId);
  if (!load) {
    throw new BadRequestError('No active loads');
  }
  const lastIndex = [...LOAD_STATUSES].length - 1;
  const {_id, state, assigned_to, logs} = load;
  if (state === LOAD_STATUSES[lastIndex]) {
    throw new BadRequestError('Already delivered');
  }
  const index = LOAD_STATUSES.indexOf(state);
  const nextIndex = index + 1;
  const nextState = LOAD_STATUSES[nextIndex];

  logs.push({
    message: `Load state changed to '${nextState}'`,
  });

  const updated = await loadsDbRepo.update({id: _id}, {state: nextState, logs});

  const newState = updated.state;

  if (newState === LOAD_STATUSES[lastIndex]) {
    logs.push({
      message: `Status changed to 'SHIPPED'`,
    });
    await loadsDbRepo.update({id: _id}, {status: 'SHIPPED', logs});
    const [truck] = await trucksService.getByFilter({assigned_to});
    const {_id: truckId} = truck;
    await trucksService.update(
        {id: truckId},
        {status: 'IS'},
    );
  }

  return newState;
};

const remove = async (load) => {
  const {id} = load;
  const loadEntity = await get(id);

  if (loadEntity.status === 'NEW') {
    await loadsDbRepo.remove(load);
  } else {
    throw new BadRequestError(`You can delete loads only with NEW status`);
  }
};

const postLoadById = async (load) => {
  const {id} = load;
  const loadEntity = await get(id);

  if (loadEntity.status === 'NEW') {
    const {logs} = loadEntity;

    logs.push({
      message: 'Posted, searching for driver',
    });

    // await loadsDbRepo.update(load, {status: 'POSTED', logs});

    const {dimensions} = loadEntity;
    const {payload} = loadEntity;

    const filters = {
      payload,
      dimensions,
    };

    // need to found free driver
    const trucksWithDrivers = await trucksService
        .getISTrucksWithDrivers(filters);

    const [truck] = trucksWithDrivers;

    if (truck) {
      // assign load to driver/truck
      const {assigned_to, _id: id} = truck;

      logs.push({
        message: `Load assigned to driver with id ${assigned_to}`,
      });

      await loadsDbRepo.update(load, {status: 'ASSIGNED', assigned_to, logs});

      // change driver status to 'OL'
      await trucksService.update({id}, {status: 'OL'});
    } else {
      logs.push({
        message:
        'Rolled back to NEW (No free driver/truck found)',
      });
      await loadsDbRepo.update(load, {status: 'NEW', logs});
      throw new BadRequestError('No free driver/truck IN SERVICE found');
    }
  } else {
    throw new BadRequestError(`You can post loads only with NEW status`);
  }
};

export default {
  create,
  getAll,
  get,
  getActiveLoad,
  update,
  updateLoadActiveState,
  remove,
  postLoadById,
};
