# RD LAB NODEJS HOMEWORK №3 UBER TRUCK APP

This app started from RD LAB NODEJS HOMEWORK №2 ([link](https://gitlab.com/IgorOsa/nodejs-hw-2)) task as a template.

## Project setup guide

Git clone this repo to your device. Then run next command in project directory to install dependencies:

`npm install`

After that you can run local server with command:

`npm start`

Default API server's adress is [localhost:8080](http://localhost:8080/api).

Swagger UI documentation and testing tool available at [/doc](http://localhost:8080/doc) endpoint.

Happy coding ;)
